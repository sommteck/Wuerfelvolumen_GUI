using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Würfelvolumen_GUI
{
    public partial class CWuerfel : Form
    {
        public CWuerfel()
        {
            // Beim Zeiger auf den Button fahren eine Tipp anzeigen
            InitializeComponent();
            ToolTip tp = new ToolTip();
            tp.SetToolTip(cmd_berechnen, "Würfelvolumen berechnen");
            tp.SetToolTip(cmd_clear, "Anzeige löschen");
            tp.SetToolTip(cmdEnde, "Programm beenden");
        }

        private void CWuerfelvolumen_Click(object sender, EventArgs e)
        {

        }

        private void cmdEnde_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_berechnen_Click(object sender, EventArgs e)
        {
            try
            {
                double seitenlaenge, volumen;                       // Variablendeklenation
                seitenlaenge = Convert.ToDouble(txt_Input.Text);    // 
                // Verarbeitung
                volumen = Math.Pow(seitenlaenge, 3);
                // Ausgabe wird in eine Textausgabe mit zwei Nachkommastellen umgewandelt
                lbl_Output.Text = "Würfelvolumen: " + volumen.ToString("F2");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_Input.Clear();
                txt_Input.Focus();
            }
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            // Der Coursor springt automatisch immer in die Textbox zurück
            txt_Input.Text = lbl_Output.Text = "";
            txt_Input.Focus();
        }
    }
}