namespace Würfelvolumen_GUI
{
    partial class CWuerfel
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.CWuerfelvolumen = new System.Windows.Forms.Label();
            this.txt_Input = new System.Windows.Forms.TextBox();
            this.cmdEnde = new System.Windows.Forms.Button();
            this.cmd_berechnen = new System.Windows.Forms.Button();
            this.lbl_Output = new System.Windows.Forms.Label();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CWuerfelvolumen
            // 
            this.CWuerfelvolumen.AutoSize = true;
            this.CWuerfelvolumen.Location = new System.Drawing.Point(41, 35);
            this.CWuerfelvolumen.Name = "CWuerfelvolumen";
            this.CWuerfelvolumen.Size = new System.Drawing.Size(97, 16);
            this.CWuerfelvolumen.TabIndex = 0;
            this.CWuerfelvolumen.Text = "Würfelvolumen";
            this.CWuerfelvolumen.Click += new System.EventHandler(this.CWuerfelvolumen_Click);
            // 
            // txt_Input
            // 
            this.txt_Input.Location = new System.Drawing.Point(182, 32);
            this.txt_Input.Name = "txt_Input";
            this.txt_Input.Size = new System.Drawing.Size(100, 22);
            this.txt_Input.TabIndex = 1;
            // 
            // cmdEnde
            // 
            this.cmdEnde.Location = new System.Drawing.Point(118, 231);
            this.cmdEnde.Name = "cmdEnde";
            this.cmdEnde.Size = new System.Drawing.Size(101, 52);
            this.cmdEnde.TabIndex = 2;
            this.cmdEnde.Text = "&Ende";
            this.cmdEnde.UseVisualStyleBackColor = true;
            this.cmdEnde.Click += new System.EventHandler(this.cmdEnde_Click);
            // 
            // cmd_berechnen
            // 
            this.cmd_berechnen.Location = new System.Drawing.Point(204, 80);
            this.cmd_berechnen.Name = "cmd_berechnen";
            this.cmd_berechnen.Size = new System.Drawing.Size(113, 62);
            this.cmd_berechnen.TabIndex = 3;
            this.cmd_berechnen.Text = "&Würfelvolumen berechnen";
            this.cmd_berechnen.UseVisualStyleBackColor = true;
            this.cmd_berechnen.Click += new System.EventHandler(this.cmd_berechnen_Click);
            // 
            // lbl_Output
            // 
            this.lbl_Output.AutoSize = true;
            this.lbl_Output.Location = new System.Drawing.Point(151, 169);
            this.lbl_Output.Name = "lbl_Output";
            this.lbl_Output.Size = new System.Drawing.Size(0, 16);
            this.lbl_Output.TabIndex = 4;
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(44, 90);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(107, 42);
            this.cmd_clear.TabIndex = 5;
            this.cmd_clear.Text = "&Anzeige löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // CWuerfel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 322);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.lbl_Output);
            this.Controls.Add(this.cmd_berechnen);
            this.Controls.Add(this.cmdEnde);
            this.Controls.Add(this.txt_Input);
            this.Controls.Add(this.CWuerfelvolumen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "CWuerfel";
            this.Text = "Würfelvolumen berechnen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CWuerfelvolumen;
        private System.Windows.Forms.TextBox txt_Input;
        private System.Windows.Forms.Button cmdEnde;
        private System.Windows.Forms.Button cmd_berechnen;
        private System.Windows.Forms.Label lbl_Output;
        private System.Windows.Forms.Button cmd_clear;
    }
}

